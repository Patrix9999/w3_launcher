#include <QtCore>
#include <QSettings>

#include "downloadmanager.h"
#include "remoteregistry.h"
#include "registrymanager.h"

DownloadManager::DownloadManager(Ui::Launcher *ui) :
    ui(ui)
{}

void DownloadManager::init()
{
    m_currentFileToDownload = 0;
    m_filesToDownloadCount = RemoteRegistry::get().registryIndices.length();

    downloadNextIndex();
}

void DownloadManager::stateDownload(QString message)
{
    ui->downloadFileProgress->resetFormat();
    ui->downloadFileProgress->setValue(0);

    ui->statusLabel->setText(message + QString::number(m_currentFileToDownload) + "/" + QString::number(m_filesToDownloadCount));
}

void DownloadManager::stateReadyToPlay()
{
    ui->downloadProgress->setValue(100);

    ui->downloadFileProgress->resetFormat();
    ui->downloadFileProgress->setValue(100);

    ui->statusLabel->setText("Gotowy do gry");
    ui->playButton->setEnabled(true);
}

QByteArray DownloadManager::getFileChecksum(QString fileName, QCryptographicHash::Algorithm hashAlgorithm)
{
    QFile file(fileName);

    if (file.open(QFile::ReadOnly))
    {
        QCryptographicHash hash(hashAlgorithm);

        if (hash.addData(&file))
            return hash.result().toHex();
    }

    return QByteArray();
}

void DownloadManager::downloadNextIndex()
{
    if (RemoteRegistry::get().registryIndices.isEmpty())
    {
        m_currentFileToDownload = 0;
        m_filesToDownloadCount = m_filesToDownload.length();

        ui->downloadProgress->setValue(0);
        downloadNextFile();

        return;
    }

    QString file = RemoteRegistry::get().registryIndices.dequeue();
    QNetworkRequest request(file);

    m_pCurrentDownload = m_manager.get(request);

    connect(m_pCurrentDownload, SIGNAL(downloadProgress(qint64, qint64)), SLOT(onDownloadProgress(qint64, qint64)));
    connect(m_pCurrentDownload, SIGNAL(finished()), SLOT(onIndexDownloadFinished()));

    ++m_currentFileToDownload;
    stateDownload("Sprawdzanie wpisu: ");
}

void DownloadManager::parseFilesFromIndex(QString json)
{
    if (!json.length())
        return;

    QJsonParseError jsonError;
    QJsonDocument doc = QJsonDocument::fromJson(json.toUtf8(), &jsonError);

    if (jsonError.error)
        return;

    QList<QVariant> list = doc.toVariant().toList();

    for (auto &fileRecord : list)
    {   
        QMap<QString, QVariant> file = fileRecord.toMap();

        if (!file["path"].toString().length() || !file["md5"].toString().length())
            continue;

        QString mapFilePath = RegistryManager::get().gameDir() + "/Maps/" + file["path"].toString();

        if (file["md5"].toString() == getFileChecksum(mapFilePath, QCryptographicHash::Md5))
            continue;

        QString baseUrl = m_pCurrentDownload->url().toString();
        baseUrl.chop(m_pCurrentDownload->url().fileName().length());

        m_filesToDownload.push_back(mFile(baseUrl, file["path"].toString(), file["md5"].toString()));
    }
}

void DownloadManager::downloadNextFile()
{
    if (m_filesToDownload.isEmpty())
    {
        stateReadyToPlay();
        return;
    }

    mFile file = m_filesToDownload.dequeue();

    QDir mapDirectory(RegistryManager::get().gameDir() + "/Maps/");
    QString mapSubDirectory(QFileInfo(file.path).path());

    if (!mapDirectory.exists(mapSubDirectory))
    {
        if (!mapDirectory.mkdir(mapSubDirectory))
        {
            downloadNextFile();
            return;
        }
    }

    m_output.setFileName(RegistryManager::get().gameDir() + "/Maps/" + file.path);

    if (!m_output.open(QIODevice::WriteOnly))
    {
        ui->downloadProgress->setValue(ui->downloadProgress->value() + 100 / m_currentFileToDownload);
        downloadNextFile();

        return;
    }

    QNetworkRequest request(file.url.toString() + file.path);
    m_pCurrentDownload = m_manager.get(request);

    connect(m_pCurrentDownload, SIGNAL(downloadProgress(qint64, qint64)), SLOT(onDownloadProgress(qint64, qint64)));
    connect(m_pCurrentDownload, SIGNAL(finished()), SLOT(onFileDownloadFinished()));
    connect(m_pCurrentDownload, SIGNAL(readyRead()), SLOT(onFileReadyRead()));

    ++m_currentFileToDownload;
    stateDownload("Pobieranie pliku: ");
}

void DownloadManager::onIndexDownloadFinished()
{
    ui->downloadFileProgress->setValue(0);

    if (!m_pCurrentDownload->error())
        parseFilesFromIndex(m_pCurrentDownload->readAll());

    downloadNextIndex();
}

void DownloadManager::onFileReadyRead()
{
     m_output.write(m_pCurrentDownload->readAll());
}

void DownloadManager::onFileDownloadFinished()
{
    ui->downloadFileProgress->setValue(0);
    m_output.close();

    downloadNextFile();
}

void DownloadManager::onDownloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    int percentage = static_cast<int>(static_cast<float>(bytesReceived) / static_cast<float>(bytesTotal) * 100);

    double MBReceived = static_cast<double>(bytesReceived) / 1048576;
    double MBTotal = static_cast<double>(bytesTotal) / 1048576;

    ui->downloadProgress->setValue(static_cast<int>(static_cast<float>(m_currentFileToDownload - 1) / static_cast<float>(m_filesToDownloadCount) * 100) + (m_currentFileToDownload + percentage) / m_filesToDownloadCount);

    ui->downloadFileProgress->setFormat(QString::number(MBReceived, 'g', 4) + " MB / " + QString::number(MBTotal, 'g', 4) + " MB");
    ui->downloadFileProgress->setValue(percentage);
}
