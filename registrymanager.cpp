#include "registrymanager.h"

RegistryManager::RegistryManager() :
    m_registry("HKEY_CURRENT_USER\\SOFTWARE\\Blizzard Entertainment\\Warcraft III", QSettings::NativeFormat)
{}

RegistryManager & RegistryManager::get()
{
    static RegistryManager instance;
    return instance;
}

QString RegistryManager::gameDir()
{
    return m_registry.value("InstallPath").toString();
}

QString RegistryManager::playerName()
{
    m_registry.beginGroup("String");
    QString value = m_registry.value("userbnet", "Player").toString();
    m_registry.endGroup();

    return value;
}

int RegistryManager::gameSpeed()
{
    m_registry.beginGroup("GamePlay");
    int value = m_registry.value("gamespeed", 1).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::animQuality()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("animquality", 2).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::colorDepth()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("colordepth", 32).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::gamma()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("gamma", 30).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::lights()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("lights", 2).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::modelDetail()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("modeldetail", 2).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::occlusion()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("occlusion", 1).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::particles()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("particles", 2).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::resWidth()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("reswidth", 800).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::resHeight()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("resheight", 600).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::spellFilter()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("spellfilter", 1).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::texColorDepth()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("texcolordepth", 32).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::texQuality()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("texquality", 2).toInt();
    m_registry.endGroup();

    return value;
}

int RegistryManager::unitShadows()
{
    m_registry.beginGroup("Video");
    int value = m_registry.value("unitshadows", 1).toInt();
    m_registry.endGroup();

    return value;
}

bool RegistryManager::ambient()
{
    m_registry.beginGroup("Sound");
    bool value = m_registry.value("ambient", 1).toBool();
    m_registry.endGroup();

    return value;
}

bool RegistryManager::doNotUseWaveOut()
{
    m_registry.beginGroup("Sound");
    bool value = m_registry.value("donotusewaveout", 0).toBool();
    m_registry.endGroup();

    return value;
}

bool RegistryManager::environmental()
{
    m_registry.beginGroup("Sound");
    bool value = m_registry.value("environmental", 1).toBool();
    m_registry.endGroup();

    return value;
}

bool RegistryManager::movement()
{
    m_registry.beginGroup("Sound");
    bool value = m_registry.value("movement", 1).toBool();
    m_registry.endGroup();

    return value;
}

bool RegistryManager::music()
{
    m_registry.beginGroup("Sound");
    bool value = m_registry.value("music", 1).toBool();
    m_registry.endGroup();

    return value;
}

int RegistryManager::musicVolume()
{
    m_registry.beginGroup("Sound");
    int value = m_registry.value("musicvolume", 6).toInt();
    m_registry.endGroup();

    return value;
}

bool RegistryManager::noMidi()
{
    m_registry.beginGroup("Sound");
    bool value = m_registry.value("nomidi", 1).toBool();
    m_registry.endGroup();

    return value;
}

bool RegistryManager::noSoundWarn()
{
    m_registry.beginGroup("Sound");
    bool value = m_registry.value("nosoundwarn", 1).toBool();
    m_registry.endGroup();

    return value;
}

bool RegistryManager::positional()
{
    m_registry.beginGroup("Sound");
    bool value = m_registry.value("positional", 1).toBool();
    m_registry.endGroup();

    return value;
}

bool RegistryManager::sfx()
{
    m_registry.beginGroup("Sound");
    bool value = m_registry.value("sfx", 1).toBool();
    m_registry.endGroup();

    return value;
}

int RegistryManager::sfxVolume()
{
    m_registry.beginGroup("Sound");
    int value = m_registry.value("sfxvolume", 7).toInt();
    m_registry.endGroup();

    return value;
}

bool RegistryManager::subtitles()
{
    m_registry.beginGroup("Sound");
    bool value = m_registry.value("subtitles", 0).toBool();
    m_registry.endGroup();

    return value;
}

void RegistryManager::setPlayerName(QString value)
{
    m_registry.beginGroup("String");
    m_registry.setValue("userbnet", value);
    m_registry.endGroup();
}

void RegistryManager::setGameSpeed(int value)
{
    m_registry.beginGroup("Gameplay");
    m_registry.setValue("gamespeed", value);
    m_registry.endGroup();
}

void RegistryManager::setAnimQuality(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("animquality", value);
    m_registry.endGroup();
}

void RegistryManager::setColorDepth(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("colordepth", value);
    m_registry.endGroup();
}

void RegistryManager::setGamma(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("gamma", value);
    m_registry.endGroup();
}

void RegistryManager::setLights(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("lights", value);
    m_registry.endGroup();
}

void RegistryManager::setModelDetail(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("modeldetail", value);
    m_registry.endGroup();
}

void RegistryManager::setOcclusion(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("occlusion", value);
    m_registry.endGroup();
}

void RegistryManager::setParticles(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("particles", value);
    m_registry.endGroup();
}

void RegistryManager::setResWidth(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("reswidth", value);
    m_registry.endGroup();
}

void RegistryManager::setResHeight(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("resheight", value);
    m_registry.endGroup();
}

void RegistryManager::setSpellFilter(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("spellfilter", value);
    m_registry.endGroup();
}

void RegistryManager::setTexColorDepth(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("texcolordepth", value);
    m_registry.endGroup();
}

void RegistryManager::setTexQuality(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("texquality", value);
    m_registry.endGroup();
}

void RegistryManager::setUnitShadows(int value)
{
    m_registry.beginGroup("Video");
    m_registry.setValue("unitshadows", value);
    m_registry.endGroup();
}

void RegistryManager::setAmbient(bool value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("ambient", value);
    m_registry.endGroup();
}

void RegistryManager::setDoNotUseWaveOut(bool value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("ambient", value);
    m_registry.endGroup();
}

void RegistryManager::setEnvironmental(bool value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("enironmental", value);
    m_registry.endGroup();
}

void RegistryManager::setMovement(bool value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("movement", value);
    m_registry.endGroup();
}

void RegistryManager::setMusic(bool value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("music", value);
    m_registry.endGroup();
}

void RegistryManager::setMusicVolume(int value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("musicvolume", value);
    m_registry.endGroup();
}

void RegistryManager::setNoMidi(bool value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("nomidi", value);
    m_registry.endGroup();
}

void RegistryManager::setNoSoundWarn(bool value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("nosoundwarn", value);
    m_registry.endGroup();
}

void RegistryManager::setPositional(bool value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("positional", value);
    m_registry.endGroup();
}

void RegistryManager::setSfx(bool value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("sfx", value);
    m_registry.endGroup();
}

void RegistryManager::setSfxVolume(int value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("sfxvolume", value);
    m_registry.endGroup();
}

void RegistryManager::setSubtitles(bool value)
{
    m_registry.beginGroup("Sound");
    m_registry.setValue("subtitles", value);
    m_registry.endGroup();
}

void RegistryManager::fillMissingKeys(QString gameDir)
{
    m_registry.setValue("InstallPath", gameDir);
    m_registry.setValue("InstallPathX", gameDir);
    m_registry.setValue("Program", gameDir + "/Warcraft III.exe");
    m_registry.setValue("ProgramX", gameDir + "/Frozen Throne.exe");
}
