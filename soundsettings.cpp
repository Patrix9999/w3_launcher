#include "soundsettings.h"
#include "ui_soundsettings.h"

#include "registrymanager.h"

#include <QDebug>

SoundSettings::SoundSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SoundSettings)
{
    ui->setupUi(this);

    setFixedSize(size());
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->ambientCheckBox->setChecked(RegistryManager::get().ambient());
    ui->doNotUseWaveOutCheckBox->setChecked(RegistryManager::get().doNotUseWaveOut());
    ui->environmentalCheckBox->setChecked(RegistryManager::get().environmental());
    ui->movementCheckBox->setChecked(RegistryManager::get().movement());
    ui->musicCheckBox->setChecked(RegistryManager::get().music());
    ui->musicVolumeSpinBox->setValue(RegistryManager::get().musicVolume());
    ui->noMidiCheckBox->setChecked(RegistryManager::get().noMidi());
    ui->noSoundWarn->setChecked(RegistryManager::get().noSoundWarn());
    ui->positionalCheckBox->setChecked(RegistryManager::get().positional());
    ui->subtitlesCheckBox->setChecked(RegistryManager::get().subtitles());
    ui->sfxCheckBox->setChecked(RegistryManager::get().sfx());
    ui->sfxVolumeSpinBox->setValue(RegistryManager::get().sfxVolume());
}

SoundSettings::~SoundSettings()
{
    delete ui;
}

void SoundSettings::on_acceptButton_clicked()
{
    RegistryManager::get().setAmbient(ui->ambientCheckBox->isChecked());
    RegistryManager::get().setDoNotUseWaveOut(ui->doNotUseWaveOutCheckBox->isChecked());
    RegistryManager::get().setEnvironmental(ui->environmentalCheckBox->isChecked());
    RegistryManager::get().setMovement(ui->movementCheckBox->isChecked());
    RegistryManager::get().setMusic(ui->musicCheckBox->isChecked());
    RegistryManager::get().setMusicVolume(ui->musicVolumeSpinBox->value());
    RegistryManager::get().setNoMidi(ui->noMidiCheckBox->isChecked());
    RegistryManager::get().setNoSoundWarn(ui->noSoundWarn->isChecked());
    RegistryManager::get().setPositional(ui->positionalCheckBox->isChecked());
    RegistryManager::get().setSubtitles(ui->subtitlesCheckBox->isChecked());
    RegistryManager::get().setSfx(ui->sfxCheckBox->isChecked());
    RegistryManager::get().setSfxVolume(ui->sfxVolumeSpinBox->value());

    close();
}
