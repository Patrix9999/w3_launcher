#ifndef SOUNDSETTINGS_H
#define SOUNDSETTINGS_H

#include <QDialog>

namespace Ui {
class SoundSettings;
}

class SoundSettings : public QDialog
{
    Q_OBJECT

public:
    explicit SoundSettings(QWidget *parent = nullptr);
    ~SoundSettings();

private slots:
    void on_acceptButton_clicked();

private:
    Ui::SoundSettings *ui;
};

#endif // SOUNDSETTINGS_H
