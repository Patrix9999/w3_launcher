#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QQueue>

#include "httpmanager.h"

class RemoteRegistry : public QObject
{
    Q_OBJECT

public:
    explicit RemoteRegistry(QObject *parent = nullptr);

    static RemoteRegistry & get();
    void load();

    QQueue<QString> registryIndices;

signals:
    void onLoad();

private slots:
    void onRequestFinished(QUrl url, QJsonDocument doc);

private:
    HttpManager m_httpMan;
};

#endif
