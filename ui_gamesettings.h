/********************************************************************************
** Form generated from reading UI file 'gamesettings.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAMESETTINGS_H
#define UI_GAMESETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GameSettings
{
public:
    QFormLayout *formLayout_3;
    QFormLayout *gameSpeedLayout;
    QLabel *gameSpeedLabel;
    QSpinBox *gameSpeedSpinBox;
    QPushButton *acceptButton;
    QFormLayout *nicknameLayout;
    QLabel *nicknameLabel;
    QLineEdit *nicknameLineEdit;

    void setupUi(QWidget *GameSettings)
    {
        if (GameSettings->objectName().isEmpty())
            GameSettings->setObjectName(QStringLiteral("GameSettings"));
        GameSettings->resize(294, 100);
        formLayout_3 = new QFormLayout(GameSettings);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        gameSpeedLayout = new QFormLayout();
        gameSpeedLayout->setObjectName(QStringLiteral("gameSpeedLayout"));
        gameSpeedLabel = new QLabel(GameSettings);
        gameSpeedLabel->setObjectName(QStringLiteral("gameSpeedLabel"));

        gameSpeedLayout->setWidget(0, QFormLayout::LabelRole, gameSpeedLabel);

        gameSpeedSpinBox = new QSpinBox(GameSettings);
        gameSpeedSpinBox->setObjectName(QStringLiteral("gameSpeedSpinBox"));

        gameSpeedLayout->setWidget(0, QFormLayout::FieldRole, gameSpeedSpinBox);


        formLayout_3->setLayout(1, QFormLayout::LabelRole, gameSpeedLayout);

        acceptButton = new QPushButton(GameSettings);
        acceptButton->setObjectName(QStringLiteral("acceptButton"));
        acceptButton->setAutoDefault(true);

        formLayout_3->setWidget(2, QFormLayout::SpanningRole, acceptButton);

        nicknameLayout = new QFormLayout();
        nicknameLayout->setObjectName(QStringLiteral("nicknameLayout"));
        nicknameLabel = new QLabel(GameSettings);
        nicknameLabel->setObjectName(QStringLiteral("nicknameLabel"));

        nicknameLayout->setWidget(0, QFormLayout::LabelRole, nicknameLabel);

        nicknameLineEdit = new QLineEdit(GameSettings);
        nicknameLineEdit->setObjectName(QStringLiteral("nicknameLineEdit"));

        nicknameLayout->setWidget(0, QFormLayout::FieldRole, nicknameLineEdit);


        formLayout_3->setLayout(0, QFormLayout::SpanningRole, nicknameLayout);

        QWidget::setTabOrder(nicknameLineEdit, gameSpeedSpinBox);
        QWidget::setTabOrder(gameSpeedSpinBox, acceptButton);

        retranslateUi(GameSettings);

        QMetaObject::connectSlotsByName(GameSettings);
    } // setupUi

    void retranslateUi(QWidget *GameSettings)
    {
        GameSettings->setWindowTitle(QApplication::translate("GameSettings", "Ustawienia gry", 0));
        gameSpeedLabel->setText(QApplication::translate("GameSettings", "Szybko\305\233\304\207 gry:", 0));
        acceptButton->setText(QApplication::translate("GameSettings", "Zaakceptuj", 0));
        nicknameLabel->setText(QApplication::translate("GameSettings", "Nick:", 0));
    } // retranslateUi

};

namespace Ui {
    class GameSettings: public Ui_GameSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAMESETTINGS_H
