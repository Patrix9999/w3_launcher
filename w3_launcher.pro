#-------------------------------------------------
#
# Project created by QtCreator 2018-04-24T14:22:41
#
#-------------------------------------------------

QT       += core gui xml network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = w3_launcher
TEMPLATE = app

RC_ICONS = resources\icon.ico

VERSION_MAJOR = 0
VERSION_MINOR = 2
VERSION_BUILD = 0

DEFINES += \
    "VERSION_MAJOR=$$VERSION_MAJOR" \
    "VERSION_MINOR=$$VERSION_MINOR" \
    "VERSION_BUILD=$$VERSION_BUILD"

VERSION = $${VERSION_MAJOR}.$${VERSION_MINOR}.$${VERSION_BUILD}

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    downloadmanager.cpp \
    launcher.cpp \
    registrymanager.cpp \
    graphsettings.cpp \
    soundsettings.cpp \
    gamesettings.cpp \
    remoteregistry.cpp \
    httpmanager.cpp

HEADERS += \
    downloadmanager.h \
    launcher.h \
    registrymanager.h \
    graphsettings.h \
    soundsettings.h \
    gamesettings.h \
    remoteregistry.h \
    httpmanager.h

FORMS += \
        launcher.ui \
    graphsettings.ui \
    soundsettings.ui \
    gamesettings.ui

RESOURCES += \
    resources.qrc

DISTFILES +=
