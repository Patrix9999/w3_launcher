#include "gamesettings.h"
#include "ui_gamesettings.h"

#include "registrymanager.h"

GameSettings::GameSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GameSettings)
{
    ui->setupUi(this);

    setFixedSize(size());
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->nicknameLineEdit->setText(RegistryManager::get().playerName());
    ui->gameSpeedSpinBox->setValue(RegistryManager::get().gameSpeed());
}

GameSettings::~GameSettings()
{
    delete ui;
}

void GameSettings::on_acceptButton_clicked()
{
    RegistryManager::get().setPlayerName(ui->nicknameLineEdit->text());
    RegistryManager::get().setGameSpeed(ui->gameSpeedSpinBox->value());

    close();
}
