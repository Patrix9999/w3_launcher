#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QtNetwork>

#include "ui_launcher.h"

class DownloadManager : public QObject
{
    Q_OBJECT

public:
    DownloadManager(Ui::Launcher *ui);

    void init();
    // UI
    void stateDownload(QString message);
    void stateReadyToPlay();
    // utility
    QByteArray getFileChecksum(QString fileName, QCryptographicHash::Algorithm hashAlgorithm);
    // handling index files
    void downloadNextIndex();
    void parseFilesFromIndex(QString json);
    // downloading files
    void downloadNextFile();

private slots:
    // index events
    void onIndexDownloadFinished();
    // file events
    void onFileReadyRead();
    void onFileDownloadFinished();
    // shared events
    void onDownloadProgress(qint64 bytesReceived, qint64 bytesTotal);

private:
    struct mFile
    {
        QUrl url;
        QString path;
        QString md5;

        mFile(QUrl _url, QString _path, QString _md5) :
            url(_url),
            path(_path),
            md5(_md5)
        {}
    };

    Ui::Launcher *ui;

    int m_currentFileToDownload;
    int m_filesToDownloadCount;

    QNetworkAccessManager m_manager;
    QNetworkReply *m_pCurrentDownload = nullptr;

    QQueue<mFile> m_filesToDownload;
    QFile m_output;
};

#endif
