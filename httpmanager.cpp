#include "httpmanager.h"

HttpManager::HttpManager(QObject *parent) :
    QObject(parent)
{
    connect(&m_manager, SIGNAL(finished(QNetworkReply *)), SLOT(onRequestFinished(QNetworkReply *)));
}

void HttpManager::post(QUrl url, QJsonDocument doc)
{
     QNetworkRequest request(url);

     request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
     m_manager.post(request, doc.toJson());
}

void HttpManager::get(QUrl url)
{
    QNetworkRequest request(url);
    m_manager.get(request);
}

void HttpManager::onRequestFinished(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());

    emit finished(reply->url(), response);
    reply->deleteLater();
}
