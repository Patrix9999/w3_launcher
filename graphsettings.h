#ifndef GRAPHSETTINGS_H
#define GRAPHSETTINGS_H

#include <QDialog>

namespace Ui {
class GraphSettings;
}

class GraphSettings : public QDialog
{
    Q_OBJECT

public:
    explicit GraphSettings(QWidget *parent = nullptr);
    ~GraphSettings();

    void setResolution(QString res);

private slots:
    void on_acceptButton_clicked();

private:
    Ui::GraphSettings *ui;
};

#endif // GRAPHSETTINGS_H
