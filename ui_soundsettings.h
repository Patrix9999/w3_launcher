/********************************************************************************
** Form generated from reading UI file 'soundsettings.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SOUNDSETTINGS_H
#define UI_SOUNDSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SoundSettings
{
public:
    QGridLayout *gridLayout;
    QCheckBox *ambientCheckBox;
    QCheckBox *noMidiCheckBox;
    QCheckBox *doNotUseWaveOutCheckBox;
    QCheckBox *noSoundWarn;
    QCheckBox *environmentalCheckBox;
    QCheckBox *positionalCheckBox;
    QCheckBox *movementCheckBox;
    QCheckBox *subtitlesCheckBox;
    QCheckBox *musicCheckBox;
    QCheckBox *sfxCheckBox;
    QLabel *musicVolumeLabel;
    QSpinBox *musicVolumeSpinBox;
    QLabel *sfxVolumeLabel;
    QSpinBox *sfxVolumeSpinBox;
    QPushButton *acceptButton;

    void setupUi(QWidget *SoundSettings)
    {
        if (SoundSettings->objectName().isEmpty())
            SoundSettings->setObjectName(QStringLiteral("SoundSettings"));
        SoundSettings->resize(305, 201);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SoundSettings->sizePolicy().hasHeightForWidth());
        SoundSettings->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(SoundSettings);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        ambientCheckBox = new QCheckBox(SoundSettings);
        ambientCheckBox->setObjectName(QStringLiteral("ambientCheckBox"));

        gridLayout->addWidget(ambientCheckBox, 0, 0, 1, 1);

        noMidiCheckBox = new QCheckBox(SoundSettings);
        noMidiCheckBox->setObjectName(QStringLiteral("noMidiCheckBox"));

        gridLayout->addWidget(noMidiCheckBox, 0, 2, 1, 1);

        doNotUseWaveOutCheckBox = new QCheckBox(SoundSettings);
        doNotUseWaveOutCheckBox->setObjectName(QStringLiteral("doNotUseWaveOutCheckBox"));

        gridLayout->addWidget(doNotUseWaveOutCheckBox, 1, 0, 1, 2);

        noSoundWarn = new QCheckBox(SoundSettings);
        noSoundWarn->setObjectName(QStringLiteral("noSoundWarn"));

        gridLayout->addWidget(noSoundWarn, 1, 2, 1, 2);

        environmentalCheckBox = new QCheckBox(SoundSettings);
        environmentalCheckBox->setObjectName(QStringLiteral("environmentalCheckBox"));
        environmentalCheckBox->setChecked(false);

        gridLayout->addWidget(environmentalCheckBox, 2, 0, 1, 1);

        positionalCheckBox = new QCheckBox(SoundSettings);
        positionalCheckBox->setObjectName(QStringLiteral("positionalCheckBox"));

        gridLayout->addWidget(positionalCheckBox, 2, 2, 1, 1);

        movementCheckBox = new QCheckBox(SoundSettings);
        movementCheckBox->setObjectName(QStringLiteral("movementCheckBox"));

        gridLayout->addWidget(movementCheckBox, 3, 0, 1, 1);

        subtitlesCheckBox = new QCheckBox(SoundSettings);
        subtitlesCheckBox->setObjectName(QStringLiteral("subtitlesCheckBox"));

        gridLayout->addWidget(subtitlesCheckBox, 3, 2, 1, 1);

        musicCheckBox = new QCheckBox(SoundSettings);
        musicCheckBox->setObjectName(QStringLiteral("musicCheckBox"));

        gridLayout->addWidget(musicCheckBox, 4, 0, 1, 1);

        sfxCheckBox = new QCheckBox(SoundSettings);
        sfxCheckBox->setObjectName(QStringLiteral("sfxCheckBox"));

        gridLayout->addWidget(sfxCheckBox, 4, 2, 1, 2);

        musicVolumeLabel = new QLabel(SoundSettings);
        musicVolumeLabel->setObjectName(QStringLiteral("musicVolumeLabel"));

        gridLayout->addWidget(musicVolumeLabel, 5, 0, 1, 1);

        musicVolumeSpinBox = new QSpinBox(SoundSettings);
        musicVolumeSpinBox->setObjectName(QStringLiteral("musicVolumeSpinBox"));
        sizePolicy.setHeightForWidth(musicVolumeSpinBox->sizePolicy().hasHeightForWidth());
        musicVolumeSpinBox->setSizePolicy(sizePolicy);
        musicVolumeSpinBox->setMaximum(100);

        gridLayout->addWidget(musicVolumeSpinBox, 5, 1, 1, 1);

        sfxVolumeLabel = new QLabel(SoundSettings);
        sfxVolumeLabel->setObjectName(QStringLiteral("sfxVolumeLabel"));

        gridLayout->addWidget(sfxVolumeLabel, 5, 2, 1, 1);

        sfxVolumeSpinBox = new QSpinBox(SoundSettings);
        sfxVolumeSpinBox->setObjectName(QStringLiteral("sfxVolumeSpinBox"));
        sizePolicy.setHeightForWidth(sfxVolumeSpinBox->sizePolicy().hasHeightForWidth());
        sfxVolumeSpinBox->setSizePolicy(sizePolicy);
        sfxVolumeSpinBox->setMaximum(100);

        gridLayout->addWidget(sfxVolumeSpinBox, 5, 3, 1, 1);

        acceptButton = new QPushButton(SoundSettings);
        acceptButton->setObjectName(QStringLiteral("acceptButton"));
        acceptButton->setAutoDefault(true);

        gridLayout->addWidget(acceptButton, 6, 0, 1, 4);

        QWidget::setTabOrder(ambientCheckBox, doNotUseWaveOutCheckBox);
        QWidget::setTabOrder(doNotUseWaveOutCheckBox, environmentalCheckBox);
        QWidget::setTabOrder(environmentalCheckBox, movementCheckBox);
        QWidget::setTabOrder(movementCheckBox, musicCheckBox);
        QWidget::setTabOrder(musicCheckBox, musicVolumeSpinBox);
        QWidget::setTabOrder(musicVolumeSpinBox, noMidiCheckBox);
        QWidget::setTabOrder(noMidiCheckBox, noSoundWarn);
        QWidget::setTabOrder(noSoundWarn, positionalCheckBox);
        QWidget::setTabOrder(positionalCheckBox, subtitlesCheckBox);
        QWidget::setTabOrder(subtitlesCheckBox, sfxCheckBox);
        QWidget::setTabOrder(sfxCheckBox, sfxVolumeSpinBox);
        QWidget::setTabOrder(sfxVolumeSpinBox, acceptButton);

        retranslateUi(SoundSettings);

        QMetaObject::connectSlotsByName(SoundSettings);
    } // setupUi

    void retranslateUi(QWidget *SoundSettings)
    {
        SoundSettings->setWindowTitle(QApplication::translate("SoundSettings", "Ustawienia d\305\272wi\304\231ku", 0));
        ambientCheckBox->setText(QApplication::translate("SoundSettings", "Otoczenie", 0));
        noMidiCheckBox->setText(QApplication::translate("SoundSettings", "Brak Midi", 0));
        doNotUseWaveOutCheckBox->setText(QApplication::translate("SoundSettings", "Nie u\305\274ywaj fali wyj\305\233cia", 0));
        noSoundWarn->setText(QApplication::translate("SoundSettings", "Brak ostrze\305\274e\305\204", 0));
        environmentalCheckBox->setText(QApplication::translate("SoundSettings", "\305\232rodowisko", 0));
        positionalCheckBox->setText(QApplication::translate("SoundSettings", "Pozycyjny", 0));
        movementCheckBox->setText(QApplication::translate("SoundSettings", "Ruch", 0));
        subtitlesCheckBox->setText(QApplication::translate("SoundSettings", "Napisy", 0));
        musicCheckBox->setText(QApplication::translate("SoundSettings", "Muzyka", 0));
        sfxCheckBox->setText(QApplication::translate("SoundSettings", "Efekty d\305\272wi\304\231kowe", 0));
        musicVolumeLabel->setText(QApplication::translate("SoundSettings", "G\305\202o\305\233no\305\233\304\207 muzyki:", 0));
        sfxVolumeLabel->setText(QApplication::translate("SoundSettings", "G\305\202o\305\233no\305\233\304\207 efekt\303\263w:", 0));
        acceptButton->setText(QApplication::translate("SoundSettings", "Zaakceptuj", 0));
    } // retranslateUi

};

namespace Ui {
    class SoundSettings: public Ui_SoundSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SOUNDSETTINGS_H
