#ifndef REGISTRYMANAGER_H
#define REGISTRYMANAGER_H

#include <QString>
#include <QSettings>

class RegistryManager
{
public:
    RegistryManager();

    static RegistryManager & get();

    QString gameDir();
    // game settings
    QString playerName();
    int gameSpeed();
    // graph settings
    int animQuality();
    int colorDepth();
    int gamma();
    int lights();
    int modelDetail();
    int occlusion();
    int particles();
    int resWidth();
    int resHeight();
    int spellFilter();
    int texColorDepth();
    int texQuality();
    int unitShadows();
    // sound settings
    bool ambient();
    bool doNotUseWaveOut();
    bool environmental();
    bool movement();
    bool music();
    int musicVolume();
    bool noMidi();
    bool noSoundWarn();
    bool positional();
    bool sfx();
    int sfxVolume();
    bool subtitles();

    // game settings
    void setGameSpeed(int value);
    void setPlayerName(QString value);
    // graph settings
    void setAnimQuality(int value);
    void setColorDepth(int value);
    void setGamma(int value);
    void setLights(int value);
    void setModelDetail(int value);
    void setOcclusion(int value);
    void setParticles(int value);
    void setResWidth(int value);
    void setResHeight(int value);
    void setSpellFilter(int value);
    void setTexColorDepth(int value);
    void setTexQuality(int value);
    void setUnitShadows(int value);
    // sound settings
    void setAmbient(bool value);
    void setDoNotUseWaveOut(bool value);
    void setEnvironmental(bool value);
    void setMovement(bool value);
    void setMusic(bool value);
    void setMusicVolume(int value);
    void setNoMidi(bool value);
    void setNoSoundWarn(bool value);
    void setPositional(bool value);
    void setSfx(bool value);
    void setSfxVolume(int value);
    void setSubtitles(bool value);

    void fillMissingKeys(QString gameDir);

private:
    QSettings m_registry;
};

#endif
