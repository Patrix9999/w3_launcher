/********************************************************************************
** Form generated from reading UI file 'launcher.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LAUNCHER_H
#define UI_LAUNCHER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Launcher
{
public:
    QWidget *centralWidget;
    QPushButton *playButton;
    QProgressBar *downloadProgress;
    QProgressBar *downloadFileProgress;
    QLabel *statusLabel;
    QLabel *versionLabel;
    QGroupBox *groupBox;
    QFormLayout *formLayout;
    QPushButton *graphButton;
    QPushButton *soundButton;
    QPushButton *gameButton;

    void setupUi(QMainWindow *Launcher)
    {
        if (Launcher->objectName().isEmpty())
            Launcher->setObjectName(QStringLiteral("Launcher"));
        Launcher->resize(750, 481);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Launcher->sizePolicy().hasHeightForWidth());
        Launcher->setSizePolicy(sizePolicy);
        Launcher->setStyleSheet(QStringLiteral(""));
        Launcher->setToolButtonStyle(Qt::ToolButtonIconOnly);
        centralWidget = new QWidget(Launcher);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy1);
        playButton = new QPushButton(centralWidget);
        playButton->setObjectName(QStringLiteral("playButton"));
        playButton->setEnabled(false);
        playButton->setGeometry(QRect(660, 430, 81, 31));
        playButton->setAutoDefault(true);
        playButton->setFlat(false);
        downloadProgress = new QProgressBar(centralWidget);
        downloadProgress->setObjectName(QStringLiteral("downloadProgress"));
        downloadProgress->setGeometry(QRect(20, 430, 631, 16));
        downloadProgress->setValue(0);
        downloadFileProgress = new QProgressBar(centralWidget);
        downloadFileProgress->setObjectName(QStringLiteral("downloadFileProgress"));
        downloadFileProgress->setGeometry(QRect(20, 450, 631, 16));
        downloadFileProgress->setValue(0);
        statusLabel = new QLabel(centralWidget);
        statusLabel->setObjectName(QStringLiteral("statusLabel"));
        statusLabel->setGeometry(QRect(26, 410, 601, 20));
        statusLabel->setAlignment(Qt::AlignCenter);
        versionLabel = new QLabel(centralWidget);
        versionLabel->setObjectName(QStringLiteral("versionLabel"));
        versionLabel->setGeometry(QRect(680, 460, 71, 20));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        versionLabel->setFont(font);
        versionLabel->setStyleSheet(QStringLiteral(""));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(490, 10, 251, 411));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy2);
        groupBox->setAlignment(Qt::AlignCenter);
        formLayout = new QFormLayout(groupBox);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        graphButton = new QPushButton(groupBox);
        graphButton->setObjectName(QStringLiteral("graphButton"));
        graphButton->setAutoDefault(true);

        formLayout->setWidget(1, QFormLayout::FieldRole, graphButton);

        soundButton = new QPushButton(groupBox);
        soundButton->setObjectName(QStringLiteral("soundButton"));
        soundButton->setAutoDefault(true);

        formLayout->setWidget(2, QFormLayout::FieldRole, soundButton);

        gameButton = new QPushButton(groupBox);
        gameButton->setObjectName(QStringLiteral("gameButton"));
        gameButton->setAutoDefault(true);

        formLayout->setWidget(3, QFormLayout::FieldRole, gameButton);

        Launcher->setCentralWidget(centralWidget);
        QWidget::setTabOrder(graphButton, soundButton);
        QWidget::setTabOrder(soundButton, playButton);

        retranslateUi(Launcher);

        playButton->setDefault(false);


        QMetaObject::connectSlotsByName(Launcher);
    } // setupUi

    void retranslateUi(QMainWindow *Launcher)
    {
        Launcher->setWindowTitle(QApplication::translate("Launcher", "Warcraft 3 - Launcher", 0));
        playButton->setText(QApplication::translate("Launcher", "Graj", 0));
        statusLabel->setText(QApplication::translate("Launcher", "Inicjalizacja...", 0));
        versionLabel->setText(QApplication::translate("Launcher", "v.0.0.0.0", 0));
        groupBox->setTitle(QApplication::translate("Launcher", "Ustawienia", 0));
        graphButton->setText(QApplication::translate("Launcher", "Grafika", 0));
        soundButton->setText(QApplication::translate("Launcher", "D\305\272wi\304\231k", 0));
        gameButton->setText(QApplication::translate("Launcher", "Gra", 0));
    } // retranslateUi

};

namespace Ui {
    class Launcher: public Ui_Launcher {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LAUNCHER_H
