#include "remoteregistry.h"

#include <QFile>
#include <QFileInfo>
#include <QDomDocument>
#include <QMessageBox>

RemoteRegistry::RemoteRegistry(QObject *parent) :
    QObject(parent)
{
    connect(&m_httpMan, SIGNAL(finished(QUrl, QJsonDocument)), SLOT(onRequestFinished(QUrl, QJsonDocument)));
}

RemoteRegistry & RemoteRegistry::get()
{
    static RemoteRegistry instance;
    return instance;
}

void RemoteRegistry::load()
{
    m_httpMan.get(QUrl("http://w3maps.000webhostapp.com/w3maps/registry.json"));
}

void RemoteRegistry::onRequestFinished(QUrl url, QJsonDocument doc)
{
    QJsonArray jsonIndicies = doc.array();

    for (auto index : jsonIndicies)
    {
        QString value = index.toString();

        if (!QUrl(value).isRelative())
           registryIndices.push_back(value);
        else
            registryIndices.push_back(QFileInfo(url.toString()).path() + "/" + value);
    }

    emit onLoad();
}
