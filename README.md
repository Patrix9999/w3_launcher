# Introduction

**w3_launcher** is a simple application made in C++ and Qt Framework (v.5.6.2) for downloading maps faster from online hosting(s) and change some game configuration.

## How to use it?

**User**

[Download the app release from repository](https://gitlab.com/Patrix9999/w3_launcher/tags)

**Developer**

First, make sure that you have Qt creator installed on your PC along with the toolsets for Qt 5.6.2. Then, clone the repository, and simply compile the project, to produce binary output.
Before running the app, check example folder, you can find there registry.json file, which must be provided by your own (check ``remoteregistry.cpp``) (it contains addresses to index.json files, which have information about uploaded map files).

## Configurating application

In the old versions of the application, there was a config.xml file, all you have to do, is to provide new tag ``<index>address/to/index.json</index>``
between ``<registry></registry>`` tags.

Now this file has been removed, because client's must to download it repeatedly.
To configure which index.json file(s) should be processed, you have to edit the address in ``remoteregistry.cpp`` file,
which is pointing to JSON array of index.json file(s) addresses, and of course recompile the application.

**Example:**

```js
[
    "http://newhosting.tk/index.json",
    "http://anotherhosting.tk/index.json"
]
```

## Generating index.json

To generate index.json file, you can simply download one of many tools, for checking
md5, and type everyting manually 

or

Use [this](https://gitlab.com/MiolQ/w3_launcher_json_gen) tool to make it even quicker. It's using polish language.


## Build with

* [Qt Creator](https://www.qt.io/download) - IDE
* [Qt Framework v.5.6.2](https://www.qt.io/) - Framework developed by Qt Group
* **MSVC 2015 x86** - Compiler tools developed by Microsoft

## License

This project is licensed under the (L)GPL v3 License - see the [LICENSE](LICENSE) file for details.