#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QMainWindow>

#include "graphsettings.h"
#include "soundsettings.h"
#include "gamesettings.h"

#include "downloadmanager.h"

namespace Ui {
class Launcher;
}

class Launcher : public QMainWindow
{
    Q_OBJECT

public:
    explicit Launcher(QWidget *parent = nullptr);
    ~Launcher();

    void loadStyle(QString path);

private slots:
    void onReadyToDownload();
    void on_playButton_clicked();
    void on_graphButton_clicked();
    void on_soundButton_clicked();
    void on_gameButton_clicked();

private:
    Ui::Launcher *ui;

    GraphSettings m_graphForm;
    SoundSettings m_soundForm;
    GameSettings m_gameForm;

    DownloadManager m_downloadMan;
};

#endif // LAUNCHER_H
