#include <QString>
#include <QFile>
#include <QPixmap>
#include <QPalette>
#include <QFileDialog>
#include <QMessageBox>

#include "launcher.h"
#include "ui_launcher.h"

#include "remoteregistry.h"
#include "registrymanager.h"

#define EXECUTABLE_NAME "Frozen Throne.exe"

Launcher::Launcher(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Launcher),

    m_downloadMan(ui)
{
    ui->setupUi(this);
    setFixedSize(size());

    ui->versionLabel->setText(" v." + QString::number(VERSION_MAJOR) + "." + QString::number(VERSION_MINOR) + "." + QString::number(VERSION_BUILD));
    loadStyle(":/resources/style.qss");

    RemoteRegistry::get().load();

    if (!RegistryManager::get().gameDir().length())
    {
        QMessageBox::warning(this, this->window()->windowTitle(),
            "Nie znaleziono folderu z grą!\n"
            "Wyznacz lokalizacje gry.");

        QString gameDir = QFileDialog::getExistingDirectory(this, "Wybierz folder z grą");
        QFile gameExe(gameDir + "/" EXECUTABLE_NAME);

        if (!gameDir.length() || !gameExe.exists())
        {
            QMessageBox::critical(this, this->window()->windowTitle(),
                "Podano błędną ścieżkę!");

            exit(0);
        }

        RegistryManager::get().fillMissingKeys(gameDir);
    }

    connect(&RemoteRegistry::get(), SIGNAL(onLoad()), SLOT(onReadyToDownload()));
}

Launcher::~Launcher()
{
    delete ui;
}

void Launcher::loadStyle(QString path)
{
    QPixmap bg(":/resources/background.jpg");
    bg = bg.scaled(size(), Qt::IgnoreAspectRatio);

    QPalette palette;
    palette.setBrush(QPalette::Background, bg);
    setPalette(palette);

    QFile file(path);

    if (!file.open(QFile::ReadOnly))
        return;

    QString styleSheet = QLatin1String(file.readAll());
    file.close();

    qApp->setStyleSheet(styleSheet);
    update();
}

void Launcher::onReadyToDownload()
{
    m_downloadMan.init();
}

void Launcher::on_playButton_clicked()
{
    QProcess::startDetached("\"" + RegistryManager::get().gameDir()+ "/" EXECUTABLE_NAME + "\"");
}

void Launcher::on_graphButton_clicked()
{
    m_graphForm.exec();
}

void Launcher::on_soundButton_clicked()
{
    m_soundForm.exec();
}

void Launcher::on_gameButton_clicked()
{
    m_gameForm.exec();
}
