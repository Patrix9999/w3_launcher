#include <QFile>
#include <QMessageBox>

#include "registrymanager.h"

#include "graphsettings.h"
#include "ui_graphsettings.h"

GraphSettings::GraphSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GraphSettings)
{
    ui->setupUi(this);

    setFixedSize(size());
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->animQualitySpinBox->setValue(RegistryManager::get().animQuality());
    ui->colorDepthSpinBox->setValue(RegistryManager::get().colorDepth());
    ui->gammaSpinBox->setValue(RegistryManager::get().gamma());
    ui->lightSpinBox->setValue(RegistryManager::get().lights());
    ui->modelDetailSpinBox->setValue(RegistryManager::get().modelDetail());
    ui->occlusionSpinBox->setValue(RegistryManager::get().occlusion());
    ui->particlesSpinBox->setValue(RegistryManager::get().particles());
    ui->texQualitySpinBox->setValue(RegistryManager::get().texQuality());
    ui->shadowsSpinBox->setValue(RegistryManager::get().unitShadows());
    ui->spellFilterSpinBox->setValue(RegistryManager::get().spellFilter());

    ui->resComboBox->setEditText(QString::number(RegistryManager::get().resWidth()) + "x" + QString::number(RegistryManager::get().resHeight()));
}

GraphSettings::~GraphSettings()
{
    delete ui;
}

void GraphSettings::setResolution(QString res)
{
    int xPos = res.indexOf('x');

    if (xPos == -1)
    {
        QMessageBox::warning(nullptr, "Źle wprowadzona rozdzielczość", "Podano zły format! Wpisz: (szerokość)x(wysokość) bez nawiasów!");
        return;
    }

    int width = res.left(xPos).toInt();
    int height = res.right(xPos).toInt();

    if (!width || !height)
        QMessageBox::warning(nullptr, "Źle wprowadzona rozdzielczość", "Podano zły format! Upewnij się, że dobrze wpisałeś szerokość i wysokość!");

    RegistryManager::get().setResWidth(width);
    RegistryManager::get().setResHeight(height);
}

void GraphSettings::on_acceptButton_clicked()
{
    RegistryManager::get().setAnimQuality(ui->animQualitySpinBox->value());
    RegistryManager::get().setColorDepth(ui->colorDepthSpinBox->value());
    RegistryManager::get().setTexColorDepth(ui->colorDepthSpinBox->value());
    RegistryManager::get().setGamma(ui->gammaSpinBox->value());
    RegistryManager::get().setLights(ui->lightSpinBox->value());
    RegistryManager::get().setModelDetail(ui->modelDetailSpinBox->value());
    RegistryManager::get().setOcclusion(ui->occlusionSpinBox->value());
    RegistryManager::get().setParticles(ui->particlesSpinBox->value());
    RegistryManager::get().setTexQuality(ui->texQualitySpinBox->value());
    RegistryManager::get().setUnitShadows(ui->shadowsSpinBox->value());
    RegistryManager::get().setSpellFilter(ui->spellFilterSpinBox->value());

    setResolution(ui->resComboBox->currentText());

    close();
}
