/********************************************************************************
** Form generated from reading UI file 'graphsettings.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GRAPHSETTINGS_H
#define UI_GRAPHSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GraphSettings
{
public:
    QGridLayout *gridLayout_3;
    QGroupBox *resGroupBox;
    QComboBox *resComboBox;
    QGridLayout *settingsLayout1;
    QLabel *animQualityLabel;
    QSpinBox *animQualitySpinBox;
    QLabel *colorDepthLabel;
    QSpinBox *colorDepthSpinBox;
    QLabel *gammaLabel;
    QSpinBox *gammaSpinBox;
    QLabel *lightLabel;
    QSpinBox *lightSpinBox;
    QLabel *modelDetailLabel;
    QSpinBox *modelDetailSpinBox;
    QLabel *occlusionLabel;
    QSpinBox *occlusionSpinBox;
    QSpinBox *spellFilterSpinBox;
    QLabel *spellFilterLabel;
    QGridLayout *gridLayout;
    QLabel *texQualityLabel;
    QLabel *particlesLabel;
    QSpinBox *particlesSpinBox;
    QSpinBox *texQualitySpinBox;
    QLabel *shadowsLabel;
    QSpinBox *shadowsSpinBox;
    QPushButton *acceptButton;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *GraphSettings)
    {
        if (GraphSettings->objectName().isEmpty())
            GraphSettings->setObjectName(QStringLiteral("GraphSettings"));
        GraphSettings->resize(309, 234);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(GraphSettings->sizePolicy().hasHeightForWidth());
        GraphSettings->setSizePolicy(sizePolicy);
        gridLayout_3 = new QGridLayout(GraphSettings);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        resGroupBox = new QGroupBox(GraphSettings);
        resGroupBox->setObjectName(QStringLiteral("resGroupBox"));
        resGroupBox->setAlignment(Qt::AlignCenter);
        resComboBox = new QComboBox(resGroupBox);
        resComboBox->setObjectName(QStringLiteral("resComboBox"));
        resComboBox->setGeometry(QRect(10, 30, 120, 18));
        resComboBox->setEditable(true);

        gridLayout_3->addWidget(resGroupBox, 1, 3, 1, 1);

        settingsLayout1 = new QGridLayout();
        settingsLayout1->setObjectName(QStringLiteral("settingsLayout1"));
        settingsLayout1->setVerticalSpacing(6);
        animQualityLabel = new QLabel(GraphSettings);
        animQualityLabel->setObjectName(QStringLiteral("animQualityLabel"));

        settingsLayout1->addWidget(animQualityLabel, 0, 0, 1, 1);

        animQualitySpinBox = new QSpinBox(GraphSettings);
        animQualitySpinBox->setObjectName(QStringLiteral("animQualitySpinBox"));
        sizePolicy.setHeightForWidth(animQualitySpinBox->sizePolicy().hasHeightForWidth());
        animQualitySpinBox->setSizePolicy(sizePolicy);
        animQualitySpinBox->setMaximum(2);

        settingsLayout1->addWidget(animQualitySpinBox, 0, 1, 1, 1);

        colorDepthLabel = new QLabel(GraphSettings);
        colorDepthLabel->setObjectName(QStringLiteral("colorDepthLabel"));

        settingsLayout1->addWidget(colorDepthLabel, 1, 0, 1, 1);

        colorDepthSpinBox = new QSpinBox(GraphSettings);
        colorDepthSpinBox->setObjectName(QStringLiteral("colorDepthSpinBox"));
        sizePolicy.setHeightForWidth(colorDepthSpinBox->sizePolicy().hasHeightForWidth());
        colorDepthSpinBox->setSizePolicy(sizePolicy);
        colorDepthSpinBox->setMaximum(32);

        settingsLayout1->addWidget(colorDepthSpinBox, 1, 1, 1, 1);

        gammaLabel = new QLabel(GraphSettings);
        gammaLabel->setObjectName(QStringLiteral("gammaLabel"));

        settingsLayout1->addWidget(gammaLabel, 2, 0, 1, 1);

        gammaSpinBox = new QSpinBox(GraphSettings);
        gammaSpinBox->setObjectName(QStringLiteral("gammaSpinBox"));
        sizePolicy.setHeightForWidth(gammaSpinBox->sizePolicy().hasHeightForWidth());
        gammaSpinBox->setSizePolicy(sizePolicy);
        gammaSpinBox->setMaximum(100);

        settingsLayout1->addWidget(gammaSpinBox, 2, 1, 1, 1);

        lightLabel = new QLabel(GraphSettings);
        lightLabel->setObjectName(QStringLiteral("lightLabel"));

        settingsLayout1->addWidget(lightLabel, 3, 0, 1, 1);

        lightSpinBox = new QSpinBox(GraphSettings);
        lightSpinBox->setObjectName(QStringLiteral("lightSpinBox"));
        sizePolicy.setHeightForWidth(lightSpinBox->sizePolicy().hasHeightForWidth());
        lightSpinBox->setSizePolicy(sizePolicy);
        lightSpinBox->setMaximum(2);

        settingsLayout1->addWidget(lightSpinBox, 3, 1, 1, 1);

        modelDetailLabel = new QLabel(GraphSettings);
        modelDetailLabel->setObjectName(QStringLiteral("modelDetailLabel"));

        settingsLayout1->addWidget(modelDetailLabel, 4, 0, 1, 1);

        modelDetailSpinBox = new QSpinBox(GraphSettings);
        modelDetailSpinBox->setObjectName(QStringLiteral("modelDetailSpinBox"));
        sizePolicy.setHeightForWidth(modelDetailSpinBox->sizePolicy().hasHeightForWidth());
        modelDetailSpinBox->setSizePolicy(sizePolicy);
        modelDetailSpinBox->setMaximum(2);

        settingsLayout1->addWidget(modelDetailSpinBox, 4, 1, 1, 1);

        occlusionLabel = new QLabel(GraphSettings);
        occlusionLabel->setObjectName(QStringLiteral("occlusionLabel"));

        settingsLayout1->addWidget(occlusionLabel, 5, 0, 1, 1);

        occlusionSpinBox = new QSpinBox(GraphSettings);
        occlusionSpinBox->setObjectName(QStringLiteral("occlusionSpinBox"));
        sizePolicy.setHeightForWidth(occlusionSpinBox->sizePolicy().hasHeightForWidth());
        occlusionSpinBox->setSizePolicy(sizePolicy);
        occlusionSpinBox->setMaximum(1);

        settingsLayout1->addWidget(occlusionSpinBox, 5, 1, 1, 1);

        spellFilterSpinBox = new QSpinBox(GraphSettings);
        spellFilterSpinBox->setObjectName(QStringLiteral("spellFilterSpinBox"));
        sizePolicy.setHeightForWidth(spellFilterSpinBox->sizePolicy().hasHeightForWidth());
        spellFilterSpinBox->setSizePolicy(sizePolicy);
        spellFilterSpinBox->setMaximum(2);

        settingsLayout1->addWidget(spellFilterSpinBox, 6, 1, 1, 1);

        spellFilterLabel = new QLabel(GraphSettings);
        spellFilterLabel->setObjectName(QStringLiteral("spellFilterLabel"));

        settingsLayout1->addWidget(spellFilterLabel, 6, 0, 1, 1);


        gridLayout_3->addLayout(settingsLayout1, 0, 1, 2, 1);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        texQualityLabel = new QLabel(GraphSettings);
        texQualityLabel->setObjectName(QStringLiteral("texQualityLabel"));

        gridLayout->addWidget(texQualityLabel, 1, 0, 1, 1);

        particlesLabel = new QLabel(GraphSettings);
        particlesLabel->setObjectName(QStringLiteral("particlesLabel"));
        sizePolicy.setHeightForWidth(particlesLabel->sizePolicy().hasHeightForWidth());
        particlesLabel->setSizePolicy(sizePolicy);

        gridLayout->addWidget(particlesLabel, 0, 0, 1, 1);

        particlesSpinBox = new QSpinBox(GraphSettings);
        particlesSpinBox->setObjectName(QStringLiteral("particlesSpinBox"));
        particlesSpinBox->setMaximum(2);

        gridLayout->addWidget(particlesSpinBox, 0, 1, 1, 1);

        texQualitySpinBox = new QSpinBox(GraphSettings);
        texQualitySpinBox->setObjectName(QStringLiteral("texQualitySpinBox"));
        texQualitySpinBox->setMaximum(2);

        gridLayout->addWidget(texQualitySpinBox, 1, 1, 1, 1);

        shadowsLabel = new QLabel(GraphSettings);
        shadowsLabel->setObjectName(QStringLiteral("shadowsLabel"));

        gridLayout->addWidget(shadowsLabel, 2, 0, 1, 1);

        shadowsSpinBox = new QSpinBox(GraphSettings);
        shadowsSpinBox->setObjectName(QStringLiteral("shadowsSpinBox"));
        shadowsSpinBox->setMaximum(1);

        gridLayout->addWidget(shadowsSpinBox, 2, 1, 1, 1);


        gridLayout_3->addLayout(gridLayout, 0, 3, 1, 1);

        acceptButton = new QPushButton(GraphSettings);
        acceptButton->setObjectName(QStringLiteral("acceptButton"));
        acceptButton->setAutoDefault(true);

        gridLayout_3->addWidget(acceptButton, 4, 3, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer, 4, 1, 1, 1);

        QWidget::setTabOrder(animQualitySpinBox, colorDepthSpinBox);
        QWidget::setTabOrder(colorDepthSpinBox, gammaSpinBox);
        QWidget::setTabOrder(gammaSpinBox, lightSpinBox);
        QWidget::setTabOrder(lightSpinBox, modelDetailSpinBox);
        QWidget::setTabOrder(modelDetailSpinBox, occlusionSpinBox);
        QWidget::setTabOrder(occlusionSpinBox, particlesSpinBox);
        QWidget::setTabOrder(particlesSpinBox, texQualitySpinBox);
        QWidget::setTabOrder(texQualitySpinBox, shadowsSpinBox);
        QWidget::setTabOrder(shadowsSpinBox, resComboBox);
        QWidget::setTabOrder(resComboBox, acceptButton);

        retranslateUi(GraphSettings);

        QMetaObject::connectSlotsByName(GraphSettings);
    } // setupUi

    void retranslateUi(QWidget *GraphSettings)
    {
        GraphSettings->setWindowTitle(QApplication::translate("GraphSettings", "Ustawienia graficzne", 0));
        resGroupBox->setTitle(QApplication::translate("GraphSettings", "Rozdzielczo\305\233\304\207:", 0));
        resComboBox->clear();
        resComboBox->insertItems(0, QStringList()
         << QApplication::translate("GraphSettings", "640x480", 0)
         << QApplication::translate("GraphSettings", "800x600", 0)
         << QApplication::translate("GraphSettings", "1024x600", 0)
         << QApplication::translate("GraphSettings", "1024x768", 0)
         << QApplication::translate("GraphSettings", "1280x720", 0)
         << QApplication::translate("GraphSettings", "1280x768", 0)
         << QApplication::translate("GraphSettings", "1280x800", 0)
         << QApplication::translate("GraphSettings", "1366x768", 0)
         << QApplication::translate("GraphSettings", "1600x768", 0)
         << QApplication::translate("GraphSettings", "1440x900", 0)
         << QApplication::translate("GraphSettings", "1280x1024", 0)
         << QApplication::translate("GraphSettings", "1600x900", 0)
         << QApplication::translate("GraphSettings", "1440x1024", 0)
         << QApplication::translate("GraphSettings", "1920x1080", 0)
        );
        animQualityLabel->setText(QApplication::translate("GraphSettings", "Jako\305\233\304\207 animacji:", 0));
        colorDepthLabel->setText(QApplication::translate("GraphSettings", "G\305\202\304\231bia kolor\303\263w:", 0));
        gammaLabel->setText(QApplication::translate("GraphSettings", "Gamma:", 0));
        lightLabel->setText(QApplication::translate("GraphSettings", "\305\232wiat\305\202o:", 0));
        modelDetailLabel->setText(QApplication::translate("GraphSettings", "Jako\305\233\304\207 modeli:", 0));
        occlusionLabel->setText(QApplication::translate("GraphSettings", "Okluzja:", 0));
        spellFilterLabel->setText(QApplication::translate("GraphSettings", "Jako\305\233\304\207 zakl\304\231\304\207:", 0));
        texQualityLabel->setText(QApplication::translate("GraphSettings", "Jako\305\233\304\207 tekstur:", 0));
        particlesLabel->setText(QApplication::translate("GraphSettings", "Cz\304\205steczki:", 0));
        shadowsLabel->setText(QApplication::translate("GraphSettings", "Cienie:", 0));
        acceptButton->setText(QApplication::translate("GraphSettings", "Zaakceptuj", 0));
    } // retranslateUi

};

namespace Ui {
    class GraphSettings: public Ui_GraphSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GRAPHSETTINGS_H
