#ifndef HTTPMANAGER_H
#define HTTPMANAGER_H

#include <QObject>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

class HttpManager : public QObject
{
    Q_OBJECT
public:
    explicit HttpManager(QObject *parent = nullptr);

    void post(QUrl url, QJsonDocument doc);
    void get(QUrl url);

signals:
    void finished(QUrl url, QJsonDocument reply);

private slots:
    void onRequestFinished(QNetworkReply *reply);

private:
    QNetworkAccessManager m_manager;
};

#endif // HTTPMANAGER_H
